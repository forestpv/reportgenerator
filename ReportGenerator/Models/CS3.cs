namespace ReportGenerator.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CS3 : DbContext
    {
        public CS3()
            : base("name=CS3")
        {
        }

        public virtual DbSet<stkhstm> stkhstms { get; set; }
        public virtual DbSet<stockm> stockms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<stkhstm>()
                .Property(e => e.warehouse)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.product)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.sequence_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.movement_reference)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.movement_source)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.transaction_type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.comments)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.from_bin_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.to_bin_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.batch_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.lot_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.nl_category)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.serial_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.user_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.val_only_adj_ind)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.spare)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stkhstm>()
                .Property(e => e.rowstamp)
                .IsFixedLength();

            modelBuilder.Entity<stockm>()
                .Property(e => e.warehouse)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.product)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alpha)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.description)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.supersession)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives01)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives02)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives03)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives04)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives05)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives06)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives07)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives08)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives09)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.alternatives10)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.unit_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.bin_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.long_description)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.old_vat_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.abc_category)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.discount)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.nominal_key)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.purchase_key)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.serial_numbers)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.analysis_a)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.analysis_b)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.analysis_c)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.queue_sequence)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.qty_decimal_places)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.mrp_batching)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.working_level)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.make_or_buy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.purchase_unit)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.selling_unit)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.bulk_item)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.drawing_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.catalogue_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.unit_length)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.unit_width)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.unit_height)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.supplier)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.batch_traceability)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.last_supplier)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.std_cost_freeze)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.reorder_freeze)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.mps_type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.spare)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.spare2)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.analysis_x_ref)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.vat_inclusive)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.stock_vat_type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.buyer_id)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.level_of_detail)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.pricing_discount)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.despatch_units)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.despatch_unit_use)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.stock_type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.packaging)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.shipping_category)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.held_flag)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.replenish_category)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.obsolete_flag)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stockm>()
                .Property(e => e.rowstamp)
                .IsFixedLength();
        }
    }
}
