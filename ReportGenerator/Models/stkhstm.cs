namespace ReportGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("scheme.stkhstm")]
    public partial class stkhstm
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(2)]
        public string warehouse { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string product { get; set; }

        public DateTime? dated { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(6)]
        public string sequence_no { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(10)]
        public string movement_reference { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(10)]
        public string movement_source { get; set; }

        public DateTime? movement_date { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(4)]
        public string transaction_type { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(60)]
        public string comments { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(10)]
        public string from_bin_number { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(10)]
        public string to_bin_number { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(10)]
        public string batch_number { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(10)]
        public string lot_number { get; set; }

        public DateTime? expiry_date { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(3)]
        public string nl_category { get; set; }

        [Key]
        [Column(Order = 12)]
        [StringLength(20)]
        public string serial_number { get; set; }

        [Key]
        [Column(Order = 13)]
        [StringLength(8)]
        public string user_id { get; set; }

        [Key]
        [Column(Order = 14)]
        public double movement_quantity { get; set; }

        [Key]
        [Column(Order = 15)]
        public double movement_cost { get; set; }

        [Key]
        [Column(Order = 16)]
        public double total_labour_cost { get; set; }

        [Key]
        [Column(Order = 17)]
        public double total_overhead_cos { get; set; }

        [Key]
        [Column(Order = 18)]
        public double sub_contract_cost { get; set; }

        [Key]
        [Column(Order = 19)]
        public double opening_balance { get; set; }

        [Key]
        [Column(Order = 20)]
        [StringLength(1)]
        public string val_only_adj_ind { get; set; }

        [Key]
        [Column(Order = 21)]
        [StringLength(47)]
        public string spare { get; set; }

        [Key]
        [Column(Order = 22, TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] rowstamp { get; set; }
    }
}
