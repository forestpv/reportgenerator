namespace ReportGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("scheme.stockm")]
    public partial class stockm
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(2)]
        public string warehouse { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string product { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(10)]
        public string alpha { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(20)]
        public string description { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(22)]
        public string supersession { get; set; }

        public DateTime? supsession_date { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(22)]
        public string alternatives01 { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(22)]
        public string alternatives02 { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(22)]
        public string alternatives03 { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(22)]
        public string alternatives04 { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(22)]
        public string alternatives05 { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(22)]
        public string alternatives06 { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(22)]
        public string alternatives07 { get; set; }

        [Key]
        [Column(Order = 12)]
        [StringLength(22)]
        public string alternatives08 { get; set; }

        [Key]
        [Column(Order = 13)]
        [StringLength(22)]
        public string alternatives09 { get; set; }

        [Key]
        [Column(Order = 14)]
        [StringLength(22)]
        public string alternatives10 { get; set; }

        [Key]
        [Column(Order = 15)]
        [StringLength(10)]
        public string unit_code { get; set; }

        [Key]
        [Column(Order = 16)]
        [StringLength(10)]
        public string bin_number { get; set; }

        public DateTime? issue_date { get; set; }

        public DateTime? stock_date { get; set; }

        public DateTime? delivery_date { get; set; }

        [Key]
        [Column(Order = 17)]
        [StringLength(40)]
        public string long_description { get; set; }

        [Key]
        [Column(Order = 18)]
        [StringLength(1)]
        public string old_vat_code { get; set; }

        [Key]
        [Column(Order = 19)]
        [StringLength(1)]
        public string abc_category { get; set; }

        [Key]
        [Column(Order = 20)]
        [StringLength(8)]
        public string discount { get; set; }

        [Key]
        [Column(Order = 21)]
        [StringLength(3)]
        public string nominal_key { get; set; }

        [Key]
        [Column(Order = 22)]
        [StringLength(3)]
        public string purchase_key { get; set; }

        [Key]
        [Column(Order = 23)]
        [StringLength(1)]
        public string serial_numbers { get; set; }

        [Key]
        [Column(Order = 24)]
        [StringLength(10)]
        public string analysis_a { get; set; }

        [Key]
        [Column(Order = 25)]
        [StringLength(10)]
        public string analysis_b { get; set; }

        [Key]
        [Column(Order = 26)]
        [StringLength(10)]
        public string analysis_c { get; set; }

        [Key]
        [Column(Order = 27)]
        [StringLength(6)]
        public string queue_sequence { get; set; }

        [Key]
        [Column(Order = 28)]
        [StringLength(1)]
        public string qty_decimal_places { get; set; }

        [Key]
        [Column(Order = 29)]
        [StringLength(1)]
        public string mrp_batching { get; set; }

        [Key]
        [Column(Order = 30)]
        [StringLength(1)]
        public string working_level { get; set; }

        [Key]
        [Column(Order = 31)]
        [StringLength(1)]
        public string make_or_buy { get; set; }

        [Key]
        [Column(Order = 32)]
        [StringLength(10)]
        public string purchase_unit { get; set; }

        [Key]
        [Column(Order = 33)]
        [StringLength(10)]
        public string selling_unit { get; set; }

        [Key]
        [Column(Order = 34)]
        [StringLength(1)]
        public string bulk_item { get; set; }

        [Key]
        [Column(Order = 35)]
        [StringLength(20)]
        public string drawing_number { get; set; }

        [Key]
        [Column(Order = 36)]
        [StringLength(20)]
        public string catalogue_number { get; set; }

        [Key]
        [Column(Order = 37)]
        [StringLength(4)]
        public string unit_length { get; set; }

        [Key]
        [Column(Order = 38)]
        [StringLength(4)]
        public string unit_width { get; set; }

        [Key]
        [Column(Order = 39)]
        [StringLength(4)]
        public string unit_height { get; set; }

        [Key]
        [Column(Order = 40)]
        [StringLength(8)]
        public string supplier { get; set; }

        [Key]
        [Column(Order = 41)]
        [StringLength(1)]
        public string batch_traceability { get; set; }

        [Key]
        [Column(Order = 42)]
        [StringLength(8)]
        public string last_supplier { get; set; }

        [Key]
        [Column(Order = 43)]
        [StringLength(1)]
        public string std_cost_freeze { get; set; }

        [Key]
        [Column(Order = 44)]
        [StringLength(1)]
        public string reorder_freeze { get; set; }

        [Key]
        [Column(Order = 45)]
        public double physical_qty { get; set; }

        [Key]
        [Column(Order = 46)]
        public double allocated_qty { get; set; }

        [Key]
        [Column(Order = 47)]
        public double back_order_qty { get; set; }

        [Key]
        [Column(Order = 48)]
        public double forward_order_qty { get; set; }

        [Key]
        [Column(Order = 49)]
        public double on_order_qty { get; set; }

        [Key]
        [Column(Order = 50)]
        public double price { get; set; }

        [Key]
        [Column(Order = 51)]
        public double weight { get; set; }

        [Key]
        [Column(Order = 52)]
        public double standard_cost { get; set; }

        [Key]
        [Column(Order = 53)]
        public double current_cost { get; set; }

        [Key]
        [Column(Order = 54)]
        public double expected_cost { get; set; }

        [Key]
        [Column(Order = 55)]
        public double economic_reorder_q { get; set; }

        [Key]
        [Column(Order = 56)]
        public double lead_time { get; set; }

        [Key]
        [Column(Order = 57)]
        public double min_stock_level { get; set; }

        [Key]
        [Column(Order = 58)]
        public double average_sales_valu { get; set; }

        [Key]
        [Column(Order = 59)]
        public double month_to_date01 { get; set; }

        [Key]
        [Column(Order = 60)]
        public double month_to_date02 { get; set; }

        [Key]
        [Column(Order = 61)]
        public double month_to_date03 { get; set; }

        [Key]
        [Column(Order = 62)]
        public double month_to_date04 { get; set; }

        [Key]
        [Column(Order = 63)]
        public double month_to_date05 { get; set; }

        [Key]
        [Column(Order = 64)]
        public double month_to_date06 { get; set; }

        [Key]
        [Column(Order = 65)]
        public double month_to_date07 { get; set; }

        [Key]
        [Column(Order = 66)]
        public double month_to_date08 { get; set; }

        [Key]
        [Column(Order = 67)]
        public double month_to_date09 { get; set; }

        [Key]
        [Column(Order = 68)]
        public double month_to_date10 { get; set; }

        [Key]
        [Column(Order = 69)]
        public double year_to_date01 { get; set; }

        [Key]
        [Column(Order = 70)]
        public double year_to_date02 { get; set; }

        [Key]
        [Column(Order = 71)]
        public double year_to_date03 { get; set; }

        [Key]
        [Column(Order = 72)]
        public double year_to_date04 { get; set; }

        [Key]
        [Column(Order = 73)]
        public double year_to_date05 { get; set; }

        [Key]
        [Column(Order = 74)]
        public double year_to_date06 { get; set; }

        [Key]
        [Column(Order = 75)]
        public double year_to_date07 { get; set; }

        [Key]
        [Column(Order = 76)]
        public double year_to_date08 { get; set; }

        [Key]
        [Column(Order = 77)]
        public double year_to_date09 { get; set; }

        [Key]
        [Column(Order = 78)]
        public double year_to_date10 { get; set; }

        [Key]
        [Column(Order = 79)]
        public double previous_sale_val1 { get; set; }

        [Key]
        [Column(Order = 80)]
        public double previous_sale_val2 { get; set; }

        [Key]
        [Column(Order = 81)]
        public double previous_sale_val3 { get; set; }

        [Key]
        [Column(Order = 82)]
        public double period_issue_qty01 { get; set; }

        [Key]
        [Column(Order = 83)]
        public double period_issue_qty02 { get; set; }

        [Key]
        [Column(Order = 84)]
        public double period_issue_qty03 { get; set; }

        [Key]
        [Column(Order = 85)]
        public double period_issue_qty04 { get; set; }

        [Key]
        [Column(Order = 86)]
        public double period_issue_qty05 { get; set; }

        [Key]
        [Column(Order = 87)]
        public double period_issue_qty06 { get; set; }

        [Key]
        [Column(Order = 88)]
        public double period_issue_qty07 { get; set; }

        [Key]
        [Column(Order = 89)]
        public double period_issue_qty08 { get; set; }

        [Key]
        [Column(Order = 90)]
        public double period_issue_qty09 { get; set; }

        [Key]
        [Column(Order = 91)]
        public double period_issue_qty10 { get; set; }

        [Key]
        [Column(Order = 92)]
        public double period_issue_qty11 { get; set; }

        [Key]
        [Column(Order = 93)]
        public double period_issue_qty12 { get; set; }

        [Key]
        [Column(Order = 94)]
        public double period_issue_qty13 { get; set; }

        [Key]
        [Column(Order = 95)]
        public double period_issue_val01 { get; set; }

        [Key]
        [Column(Order = 96)]
        public double period_issue_val02 { get; set; }

        [Key]
        [Column(Order = 97)]
        public double period_issue_val03 { get; set; }

        [Key]
        [Column(Order = 98)]
        public double period_issue_val04 { get; set; }

        [Key]
        [Column(Order = 99)]
        public double period_issue_val05 { get; set; }

        [Key]
        [Column(Order = 100)]
        public double period_issue_val06 { get; set; }

        [Key]
        [Column(Order = 101)]
        public double period_issue_val07 { get; set; }

        [Key]
        [Column(Order = 102)]
        public double period_issue_val08 { get; set; }

        [Key]
        [Column(Order = 103)]
        public double period_issue_val09 { get; set; }

        [Key]
        [Column(Order = 104)]
        public double period_issue_val10 { get; set; }

        [Key]
        [Column(Order = 105)]
        public double period_issue_val11 { get; set; }

        [Key]
        [Column(Order = 106)]
        public double period_issue_val12 { get; set; }

        [Key]
        [Column(Order = 107)]
        public double period_issue_val13 { get; set; }

        [Key]
        [Column(Order = 108)]
        public double standard_labour { get; set; }

        [Key]
        [Column(Order = 109)]
        public double standard_overhead { get; set; }

        [Key]
        [Column(Order = 110)]
        public double standard_material { get; set; }

        [Key]
        [Column(Order = 111)]
        public double standard_subcontra { get; set; }

        [Key]
        [Column(Order = 112)]
        public double mrp_reorder_roundi { get; set; }

        [Key]
        [Column(Order = 113)]
        public double purchase_factor { get; set; }

        [Key]
        [Column(Order = 114)]
        public double selling_factor { get; set; }

        [Key]
        [Column(Order = 115)]
        public double safety_stock_level { get; set; }

        [Key]
        [Column(Order = 116)]
        public double material_on_cost { get; set; }

        [Key]
        [Column(Order = 117)]
        public double labour_on_cost { get; set; }

        [Key]
        [Column(Order = 118)]
        public double overhead_on_cost { get; set; }

        [Key]
        [Column(Order = 119)]
        public double maximum_stock_leve { get; set; }

        [Key]
        [Column(Order = 120)]
        public double shelf_life { get; set; }

        [Key]
        [Column(Order = 121)]
        public double stock_length { get; set; }

        [Key]
        [Column(Order = 122)]
        public double stock_width { get; set; }

        [Key]
        [Column(Order = 123)]
        public double stock_height { get; set; }

        [Key]
        [Column(Order = 124)]
        public double current_material { get; set; }

        [Key]
        [Column(Order = 125)]
        public double current_labour { get; set; }

        [Key]
        [Column(Order = 126)]
        public double current_overhead { get; set; }

        [Key]
        [Column(Order = 127)]
        public double current_subcontrac { get; set; }

        [Key]
        [Column(Order = 128)]
        public double uninspected_qty { get; set; }

        [Key]
        [Column(Order = 129)]
        public double period_demand01 { get; set; }

        [Key]
        [Column(Order = 130)]
        public double period_demand02 { get; set; }

        [Key]
        [Column(Order = 131)]
        public double period_demand03 { get; set; }

        [Key]
        [Column(Order = 132)]
        public double period_demand04 { get; set; }

        [Key]
        [Column(Order = 133)]
        public double period_demand05 { get; set; }

        [Key]
        [Column(Order = 134)]
        public double period_demand06 { get; set; }

        [Key]
        [Column(Order = 135)]
        public double period_demand07 { get; set; }

        [Key]
        [Column(Order = 136)]
        public double period_demand08 { get; set; }

        [Key]
        [Column(Order = 137)]
        public double period_demand09 { get; set; }

        [Key]
        [Column(Order = 138)]
        public double period_demand10 { get; set; }

        [Key]
        [Column(Order = 139)]
        public double period_demand11 { get; set; }

        [Key]
        [Column(Order = 140)]
        public double period_demand12 { get; set; }

        [Key]
        [Column(Order = 141)]
        public double period_demand13 { get; set; }

        [Key]
        [Column(Order = 142)]
        public double safety_days { get; set; }

        [Key]
        [Column(Order = 143)]
        public double review_days { get; set; }

        [Key]
        [Column(Order = 144)]
        public double reorder_days { get; set; }

        [Key]
        [Column(Order = 145)]
        public double pr_yield { get; set; }

        [Key]
        [Column(Order = 146)]
        public double setup_cost { get; set; }

        [Key]
        [Column(Order = 147)]
        public double dspare01 { get; set; }

        [Key]
        [Column(Order = 148)]
        public double dspare02 { get; set; }

        [Key]
        [Column(Order = 149)]
        public double dspare03 { get; set; }

        [Key]
        [Column(Order = 150)]
        public double dspare04 { get; set; }

        [Key]
        [Column(Order = 151)]
        public double dspare05 { get; set; }

        [Key]
        [Column(Order = 152)]
        public double dspare06 { get; set; }

        [Key]
        [Column(Order = 153)]
        public double dspare07 { get; set; }

        [Key]
        [Column(Order = 154)]
        public double dspare08 { get; set; }

        [Key]
        [Column(Order = 155)]
        public double dspare09 { get; set; }

        [Key]
        [Column(Order = 156)]
        public double dspare10 { get; set; }

        [Key]
        [Column(Order = 157)]
        public double inspection_yield { get; set; }

        [Key]
        [Column(Order = 158)]
        public double holding_cost { get; set; }

        [Key]
        [Column(Order = 159)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int inspect_cycle { get; set; }

        [Key]
        [Column(Order = 160)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int batch_receipts { get; set; }

        [Key]
        [Column(Order = 161)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int lowest_where_used { get; set; }

        public DateTime? last_revalue { get; set; }

        [Key]
        [Column(Order = 162)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int last_where_used_ru { get; set; }

        [Key]
        [Column(Order = 163)]
        public double sub_oncost_ { get; set; }

        [Key]
        [Column(Order = 164)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int spare_integer { get; set; }

        [Key]
        [Column(Order = 165)]
        public double stock_take_toleran { get; set; }

        [Key]
        [Column(Order = 166)]
        public double weight_low { get; set; }

        [Key]
        [Column(Order = 167)]
        [StringLength(1)]
        public string mps_type { get; set; }

        [Key]
        [Column(Order = 168)]
        [StringLength(6)]
        public string spare { get; set; }

        [Key]
        [Column(Order = 169)]
        public double wholesale_price { get; set; }

        [Key]
        [Column(Order = 170)]
        public double worder_allocations { get; set; }

        [Key]
        [Column(Order = 171)]
        [StringLength(48)]
        public string spare2 { get; set; }

        [Key]
        [Column(Order = 172)]
        [StringLength(3)]
        public string analysis_x_ref { get; set; }

        [Key]
        [Column(Order = 173)]
        [StringLength(1)]
        public string vat_inclusive { get; set; }

        [Key]
        [Column(Order = 174)]
        [StringLength(10)]
        public string stock_vat_type { get; set; }

        [Key]
        [Column(Order = 175)]
        [StringLength(8)]
        public string buyer_id { get; set; }

        [Key]
        [Column(Order = 176)]
        [StringLength(1)]
        public string level_of_detail { get; set; }

        [Key]
        [Column(Order = 177)]
        [StringLength(8)]
        public string pricing_discount { get; set; }

        [Key]
        [Column(Order = 178)]
        [StringLength(10)]
        public string despatch_units { get; set; }

        [Key]
        [Column(Order = 179)]
        [StringLength(1)]
        public string despatch_unit_use { get; set; }

        [Key]
        [Column(Order = 180)]
        [StringLength(1)]
        public string stock_type { get; set; }

        [Key]
        [Column(Order = 181)]
        [StringLength(10)]
        public string packaging { get; set; }

        [Key]
        [Column(Order = 182)]
        [StringLength(10)]
        public string shipping_category { get; set; }

        [Key]
        [Column(Order = 183)]
        [StringLength(1)]
        public string held_flag { get; set; }

        [Key]
        [Column(Order = 184)]
        [StringLength(3)]
        public string replenish_category { get; set; }

        [Key]
        [Column(Order = 185)]
        [StringLength(1)]
        public string obsolete_flag { get; set; }

        [Key]
        [Column(Order = 186, TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] rowstamp { get; set; }
    }
}
