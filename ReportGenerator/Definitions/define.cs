﻿//using ForestServices.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ForestServices.Definitions
{
    public enum TransactionType
    {
        Add,
        Update
    }

    public enum site
    {
        [Description("Hartlebury")]
        Hartlebury,
        [Description("Clows Top")]
        Clows_Top,
        [Description("Lockerbie")]
        Lockerbie,
        [Description("Sennybridge")]
        Sennybridge,
        [Description("Worcester")]
        Worcester,
        [Description("M & M")]
        M_and_M

    }

    
    public enum ItemType
    {
        [Description("report")]
        report,
        [Description("function")]
        function
    }

    
    public enum AdditionalFuntions
    {
        [Description("Send Customer Emails")]
        SendCustomerEmails
    }

    public enum MapPaths
    {
        [Description("~/App_Data/ReportGroups.xml")]
        ReportGroups,
        [Description("~/App_Data/FunctionGroups.xml")]
        FunctionGroups,
        [Description("~/App_Data/AllAvailableReports.xml")]
        AllAvailableReports,
        [Description("~/App_Data/AllAvailableFunctions.xml")]
        AllAvailableFunctions,
        [Description("http://lego/SendCustomerEmails/SendEmails.aspx")]
        SendEmailsPath,
        [Description("http://lego/SageImport/ImportExcelFile?ForestServices=true&version=demo")]
        ImportToSageDemo,
        [Description("http://lego/SageImport/ImportExcelFile?ForestServices=true&version=live")]
        ImportToSageLive,
        [Description("http://lego/ProductBible")]
        ViewProductBible,
        [Description("http://lego/ImportBible/ImportProductBible/")]
        ImportProductBible,
        [Description("http://lego/DeleteDuplicates/Default.aspx?ForestServices=true")]
        Delete_duplicates_for_AutoSAR,
        [Description("http://lego/ReassignCarrier/")]
        ReassignCarrierOnSpecifiedDate,
        [Description("http://lego/Update_H_Numbers/")]
        Update_H_Numbers,
        [Description("http://lego/UpdatePriceBook?ForestServices=true")]
        UpdatePriceBook,
        [Description("http://stork/LIVESymphonySAR/")]
        SAR, 
        [Description("http://lego/ImportPADProductAvailabilityFile/")]
        Upload_PAD_product_availability_file,
        [Description("http://lego/QlikviewUploadStockForecast/ImportExcelFile/")]
        Upload_Stock_Forecast,
        [Description("http://lego/SpareParts/")]
        SpareParts,
        [Description("http://lego/Pallets/")]
        Pallets 
    }


    //public class ParameterMapping
    //{
    //    public static Helper.FilterCollection GetParameterList(string SprocName)
    //    {

    //        Helper.FilterCollection parameters = new Helper.FilterCollection();
    //        Helper.FilterObject param;

    //        switch (SprocName)
    //        {
    //            case "sp_All_Claims_By_Store":
    //                param = new Helper.FilterObject();
    //                param.filterName = "customer_account";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Customer account: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Delivery_Note_Invoicing":
    //                param = new Helper.FilterObject();
    //                param.filterName = "order_no";
    //                param.size = 20;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Order no.: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Stockist_Search":
    //                param = new Helper.FilterObject();
    //                param.filterName = "criteria";
    //                param.size = 5000;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 200;
    //                param.prompt = "Enter search text: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Stock_Enquiry":
    //                param = new Helper.FilterObject();
    //                param.filterName = "product_code";
    //                param.size = 30;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Product code: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Spare_part_from_order_number":
    //                param = new Helper.FilterObject();
    //                param.filterName = "order_number";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Order number: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Spare_part_at_given_status":
    //                param = new Helper.FilterObject();
    //                param.filterName = "status";
    //                param.size = 2;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Status: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Incoming_HD_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "minus_days";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Minus days: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Outstanding_on_ledger2":
    //                param = new Helper.FilterObject();
    //                param.filterName = "customer_account";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Customer account: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Back_orders_for_selected_day":
    //                param = new Helper.FilterObject();
    //                param.filterName = "selected_day";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Selected day (eg. 'M' or 'TH' or 'M/T/W/TH/F'): ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_B_and_Q_Non_Carrier_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Z044_AND_Z046_FOC":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Enter Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_TP_Quantity_Check":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Panel_Report_Peter":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_lost_orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Enter Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_MagmaErrors":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Retrieve all Magma Errors after date :";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_SAMS_NEW_FOC_REPORT":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Enter Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Shedstore_FOC_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Enter Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_New_Linked_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "order_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Order Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_HD_order_value":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_HD_SAM":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DATE";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Order_Value":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Travis_Perkins_Back_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Tuffnells_Orders_New":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_required";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date required: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Alistairs_Information_ss":
    //                param = new Helper.FilterObject();
    //                param.filterName = "dated";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Enter dated: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_B_and_Q_Store_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Number_Of_Forest_Orders_Placed":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_HD_order_value_DDH":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date Entered: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_order_report":
    //                param = new Helper.FilterObject();
    //                param.filterName = "order_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Order Date: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_OrdersByUserByDate":
    //                param = new Helper.FilterObject();
    //                param.filterName = "order_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "All Orders Since:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "Id";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListOrdersByUserByDate";
    //                param.dataType = "String";
    //                param.defaultValue = "0";
    //                param.prompt = "Select User:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Payment_Category":
    //                param = new Helper.FilterObject();
    //                param.filterName = "credit_category";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListPayment_Category";
    //                param.dataType = "int";
    //                param.defaultValue = "0";
    //                param.prompt = "With Payment Category ?:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "info_label";
    //                param.size = 400;
    //                param.controlWidth = 400;
    //                param.type = "InfoLabel";
    //                param.dataType = "String";

    //                string strSpaces = "&nbsp;&nbsp;&nbsp;&nbsp;";


    //                param.prompt = @"<font size=3 color=#005543><br><br>Categories:  A: E30" + strSpaces + "B: E45" + strSpaces + "C: E60" + strSpaces + "D: E75" + strSpaces + "E: I45" + strSpaces + "F: I77" + strSpaces + "G: I60" + strSpaces + "H: I49<br><br></font>";
    //                parameters.paramList.Add(param);


    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_JG_report_by_class_by_week":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_received";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Date received: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Argos_Invoices":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_NewAccountsSetUpBetweenDates":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Fetch_CustomerOrderNumber2_ForCustomer":
    //                param = new Helper.FilterObject();
    //                param.filterName = "CustomerBranchCode";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer branch code:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;


    //            case "sp_Sales_Invoicing":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;                    

    //            case "sp_GetOrdersProcessedToReconcile":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "DateCreated";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ShortName";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer Short Name:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Worst_Offending_spare_parts":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "List_of_H_Numbers_date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "List of H Numbers date entered:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "List_of_H_Numbers_date_entered";
    //                parameters.filterOperator = "GreaterThanOrEqual";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Worst_Offending_spare_parts_detail":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "List_of_H_Numbers_date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "List of H Numbers date entered:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "List_of_H_Numbers_date_entered";
    //                parameters.filterOperator = "GreaterThanOrEqual";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Web_KPI_orders_processed":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "account";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Account: ";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Web_KPI_spare_parts_processed":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "account";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Account: ";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Shedstore_Invoice_New":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_TP_Warrington_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Parcelnet_Charges_GRATTANS":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Parcelnet_Charges_JD_Williams":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_MCP_Forest_Faxes":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_1";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 1:";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_MCP_Multiples":
    //                param = new Helper.FilterObject();
    //                param.filterName = "Del_day_1";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "DEL DAY 1:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "Del_day_2";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "DEL DAY 2:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "Del_day_3";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "DEL DAY 3:";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_MCP_Forest":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_1";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 1:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_2";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 2:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_3";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 3:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_4";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 4:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_5";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 5:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_6";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 6:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_7";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 7:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_8";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 8:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_9";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "ENTER DEL DAY 9:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_LH_New_product_report":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered_start";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered_end";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Home_Delivery_Total_KPI":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_required";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Home_Delivery_incoming_orders_for_a_week":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_received";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_DDHOrderSelection":

    //                param = new Helper.FilterObject();
    //                param.filterName = "FromDate";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "From Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "ToDate";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "To Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_received";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "DDHEligibility";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 20;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = null;
    //                param.prompt = "DDH Eligibility:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "AreaCode";
    //                param.size = 250;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = null;
    //                param.prompt = "AreaCode:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;


    //            case "sp_credits_to_be_matched_new":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_Inv_Crn_Jrn";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Inv Crn Jrn:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "customer1";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer 1:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "customer2";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer 2:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "account_number";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Account number:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_Wickes_Stores_Not_Ordering":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Date Entered:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_1st";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Enter 1st day:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_2nd";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Enter 2nd day:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_3rd";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Enter 3rd day:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_4th";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Enter 4th day:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_5th";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Enter 5th day:";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;


    //            case "rpt_cx_processing":
    //                param = new Helper.FilterObject();
    //                param.filterName = "customer";
    //                param.size = 8;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = null;
    //                param.prompt = "Customer:";
    //                parameters.paramList.Add(param);
                    
    //                param = new Helper.FilterObject();
    //                param.filterName = "batch_item_no";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = null;
    //                param.prompt = "Batch item no:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "transaction_item";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                //param.defaultValue = "";
    //                param.prompt = "Transaction item:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "allocated_date";
    //                param.size = 8;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                //param.defaultValue = "";
    //                param.prompt = "Allocated date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "second_ref";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                //param.defaultValue = "";
    //                param.prompt = "Second ref:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;


    //            case "sp_Outstanding_on_ledger":
    //                    param = new Helper.FilterObject();
    //                param.filterName = "enter_Inv_Crn_Jrn";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Inv Crn Jrn:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "customer1";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer 1:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "customer2";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Customer 2:";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Amazon_and_Ebay_Z044_Orders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;                    

    //            case "sp_OrdersCancelledThisYear":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_OutOfStockOrdersByPlant":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_ListSymphonyProducts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_ByTheWay":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.cs3liveApp01ConnectionString);//any changes need to be made to cs3live sp_ByTheWay
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_GetTufnellsLettersToRemove":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);//any changes need to be made to cs3live sp_ByTheWay
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Awaiting_Despatch_Spare_Parts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Back_Order_Spare_Parts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Below_MCP_orders_for_transport":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_BQ_orders_over_14_days":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.cs3liveConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_CheckCXProcessingDemo":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_CheckCXProcessingLive":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Check_HD_Code_in_Sage_and_Symphony":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Combine_Physical_Stock_WH02_WH21_AndAllocations":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Combine_Physical_Stock_WH02_WH21_AndAllocationsTest":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;               
    //            case "sp_Credit_held_spare_parts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_credit_stopped_orders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_different_H_numbers_in_order_section":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_different_H_numbers_for_an_order_number_in_Sage":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Get_OnHoldAccounts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Customer_addresses_for_mailouts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Customer_by_delivery_day":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Dennisons":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_DDHOrderSelectionNoDeliveryDate":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Direct_delivery_detail":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_DuplicatedOrders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_DuplicatedProductsOnOrders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Forest_Customer_List":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Forest_Orders_For_Bulk":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Forward_Order_Exception_Report":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Wyevale_status_5_held_orders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Home_Delivery_OOS_report":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Homebase_orders_over_28_days":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_lost_orders_for_mcp":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_NewAccountsSetUpInLast12Months":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_New_Home_Delivery_Summary_OOS":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_New_Store_HD_out_of_stock":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Home_Delivery_OOS":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Price_lists_versus_standard_selling_price":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Product_Images":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_reconcilliation":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_RemittanceCheckerRUN":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Require_Call_from_Installation_Team":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_sales_KPIS_2":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Spare_part_back_order":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Spare_part_back_order_new":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Spare_part_orders_held":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Spare_part_overview":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_spare_parts":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Split_Bundle_Line_Check":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Tillington_Back_Order_Report":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_TP_Warrington_Back_Orders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_TuffnellsUnallocatedDeliveryDates":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Vickys_Query":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh02_Stock_Movement_Report":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_decking_components":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_decking_fixings":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_lap_panels":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_split_back_order":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_summary":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_summer_houses":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_total_back_order":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.cs3liveConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_wh21_total_back_order_summary":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_Wickes_Ordering":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            //case "sp_Wickes_Ordering1":
    //            //    parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //            //    parameters.storedProcHasParameters = false;
    //            //    break;
    //            case "rpt_WH88_Stock_With_No_Orders":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;
    //            case "sp_WhosLocking":
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                parameters.storedProcHasParameters = false;
    //                break;


    //            case "sp_UpdateBundle":
    //                param = new Helper.FilterObject();
    //                param.filterName = "product";
    //                param.size = 300;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Product: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                param = new Helper.FilterObject();
    //                param.filterName = "stock_type";
    //                param.size = 8;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListStockType";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Stock Type:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "warehouse";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListWarehouse";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Warehouse:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "db";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListDB";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Database:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;


    //            case "rpt_StockReport_MM":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ProductGroup";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListProductGroupStockReport";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Product Group:";
    //                parameters.paramList.Add(param);
                    
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
                    
    //                break;


    //            case "sp_Stock_Enquiry_By_Product_Group":
    //                param = new Helper.FilterObject();
    //                param.filterName = "product_group";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListAnalysisABC";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Product Group:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "product_group_value";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListProductGroupValues";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Product Group Value:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;


    //            case "sp_Wickes_Ordering1":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Date entered:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;



    //            case "rpt_Obsolete_Stock":
    //                //parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                //parameters.storedProcHasParameters = false;


    //                param = new Helper.FilterObject();
    //                param.filterName = "obsolete";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownList";
    //                param.dataType = "String";
    //                param.defaultValue = "Y";
    //                param.prompt = "Is Obsolete:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

                    

    //                break;


    //            case "sp_products_and_side_line_bins":
    //                //parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                //parameters.storedProcHasParameters = false;


    //                param = new Helper.FilterObject();
    //                param.filterName = "obsolete";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListForSideLineBins";
    //                param.dataType = "String";
    //                param.defaultValue = "all";
    //                param.prompt = "please select:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;





    //            case "sp_Chinese_Planters":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_required";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Date Required:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "Equal";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = false;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Log_Cabin_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Enter date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "GreaterThanOrEqual";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = false;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;


    //            case "sp_Display_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_entered";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Date entered:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_despatched";
    //                parameters.filterOperator = "GreaterThanOrEqual";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "product";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Product:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;

    //            case "sp_Display_Orders2":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_despatched";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Date despatched:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_despatched";
    //                parameters.filterOperator = "GreaterThanOrEqual";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "product";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.defaultValue = "";
    //                param.prompt = "Product:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);

    //                break;
    //            case "sp_Calls_for_Easter":
    //                param = new Helper.FilterObject();
    //                param.filterName = "customer_account";
    //                param.size = 8;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Customer account: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Stock_Report_Showing_DELGRP_and_DELNTSeq":
    //                param = new Helper.FilterObject();
    //                param.filterName = "product_group";
    //                param.size = 5;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListProductGroupValuesAt21";
    //                param.dataType = "String";
    //                //param.defaultValue = "ALL";
    //                param.prompt = "Product Group:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Customer_Status_report":
    //                param = new Helper.FilterObject();
    //                param.filterName = "customer_account";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Customer account: ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Day_Book":
    //                param = new Helper.FilterObject();
    //                param.filterName = "delday";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Delivery day (eg. 'M' or 'TH' or 'M/T/W/TH/F') : ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Forest_Split_Back_Orders":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ENTER_DEL_DAY_1";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "Delivery day (eg. 'M' or 'TH' or 'M/T/W/TH/F') : ";
    //                parameters.paramList.Add(param);
    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Asda_Invoicing":

    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "invoice_date";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Gavins_Reconcilliation":
    //                param = new Helper.FilterObject();
    //                param.filterName = "date_despatched_start";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "date_despatched_end";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_despatched";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;


    //            case "sp_GroupReports_SupplierPaymentsBySupplierType_A":
    //                param = new Helper.FilterObject();
    //                param.filterName = "duedate";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Due Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "supplierTypeFrom";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListSupplierType";
    //                param.dataType = "int";
    //                param.defaultValue = "1";
    //                param.prompt = "Supplier Type From:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "supplierTypeTo";
    //                param.size = 20;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListSupplierType";
    //                param.dataType = "int";
    //                param.defaultValue = "1";
    //                param.prompt = "Supplier Type To:";
    //                parameters.paramList.Add(param);


    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_ProductBibleByProductGroup":
    //                param = new Helper.FilterObject();
    //                param.filterName = "ProductGroup";
    //                param.size = 30;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "DropDownListProductGroup";
    //                param.dataType = "int";
    //                param.defaultValue = "1";
    //                param.prompt = "Product Group:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;
    //            case "sp_GroupReports_SupplierPaymentsBySupplierType_B":
    //                param = new Helper.FilterObject();
    //                param.filterName = "duedate";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Due Date:";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_ALL_Z044":
    //                param = new Helper.FilterObject();
    //                param.filterName = "enter_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "Retrieve all Z044 orders after date :";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Alistairs_Information":

    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "stkhstm_dated";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_Argos_Order_And_Tetra_Numbers":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "date_entered";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //            case "sp_SymphonyOrdersBetweenDates":
    //                param = new Helper.FilterObject();
    //                param.filterName = "start_date";
    //                param.size = 10;
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today.AddDays(-1)";
    //                param.prompt = "Start Date:";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "end_date";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "DateTime";
    //                param.defaultValue = "=Today()";
    //                param.prompt = "End Date:";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                parameters.filterExpression = "OrderDate";
    //                parameters.filterOperator = "Between";
    //                parameters.paramList.Add(param);

    //                param = new Helper.FilterObject();
    //                param.filterName = "BranchCode";
    //                param.size = 10;
    //                param.type = "TextBox";
    //                param.dataType = "String";
    //                param.EventName = "TextChanged";
    //                param.controlWidth = 100;
    //                param.prompt = "BranchCode: ";
    //                parameters.paramList.Add(param);

    //                parameters.storedProcHasParameters = true;
    //                parameters.connectionString = GetMappings.GetEnumDescription(GetMappings.ConnectionStrings.ForestServicesConnectionString);
    //                break;

    //        }



    //        return parameters;
    //    }
    //}

    public class GetMappings
    {

        public enum mapping
        {
            [Description("All Claims By Store")]
            sp_All_Claims_By_Store,
             [Description("All Z044 orders")]
            sp_ALL_Z044,
            [Description("DES check")]
            sp_ByTheWay,
            [Description("Awaiting Despatch Spare Parts")]
            sp_Awaiting_Despatch_Spare_Parts,
            [Description("Orders Cancelled This Year")]
            sp_OrdersCancelledThisYear,
            [Description("Out Of Stock Orders By Plant")]
            sp_OutOfStockOrdersByPlant,
            [Description("Argos Invoices")]
            sp_Argos_Invoices,
            [Description("Chinese Planters")]
            sp_Chinese_Planters,
            [Description("Asda Invoicing")]
            sp_Asda_Invoicing,
            [Description("Alistair's Information")]
            sp_Alistairs_Information,
            [Description("Amazon and Ebay Z044 Orders")]
            sp_Amazon_and_Ebay_Z044_Orders,
            [Description("Argos Order And Tetra Numbers")]
            sp_Argos_Order_And_Tetra_Numbers,
            [Description("B and Q Non-Carrier Orders")]
            sp_B_and_Q_Non_Carrier_Orders,
            [Description("B and Q Store Orders")]
            sp_B_and_Q_Store_Orders,
            [Description("Back Order Spare Parts")]
            sp_Back_Order_Spare_Parts,
            [Description("Below MCP orders for transport")]
            sp_Below_MCP_orders_for_transport,
            [Description("B and Q orders over 14 days")]
            sp_BQ_orders_over_14_days,
            [Description("Calls for Easter")]
            sp_Calls_for_Easter,
            [Description("Check CX Processing in Demo")]
            sp_CheckCXProcessingDemo,
            [Description("Check CX Processing in Live")]
            sp_CheckCXProcessingLive,
            [Description("Check HD Codes in Sage and Symphony")]
            sp_Check_HD_Code_in_Sage_and_Symphony,
            [Description("Combine Physical Stock from WH02 and WH21 and Allocations")]
            sp_Combine_Physical_Stock_WH02_WH21_AndAllocations,
            [Description("Combine Physical Stock from WH02 and WH21 and Allocations Test")]
            sp_Combine_Physical_Stock_WH02_WH21_AndAllocationsTest,
            [Description("Credit held spare parts")]
            sp_Credit_held_spare_parts,
            [Description("Credit stopped orders")]
            sp_credit_stopped_orders,
            [Description("Credits to be matched")]
            sp_credits_to_be_matched_new,
            [Description("Customer addresses for mailouts")]
            sp_Customer_addresses_for_mailouts,
            [Description("Customer by delivery day")]
            sp_Customer_by_delivery_day,
            [Description("Customer Status")]
            sp_Customer_Status_report,
            [Description("Day Book")]
            sp_Day_Book,
            [Description("Delivery Note Invoicing")]
            sp_Delivery_Note_Invoicing,
            [Description("DDH Order Selection")]
            sp_DDHOrderSelection,
            [Description("DDH Order Selection No Delivery Date")]
            sp_DDHOrderSelectionNoDeliveryDate,
            [Description("Dennisons")]
            sp_Dennisons,
            [Description("Different H numbers in order section")]
            sp_different_H_numbers_in_order_section,
[Description("Different H numbers for an order number in Sage")]
            sp_different_H_numbers_for_an_order_number_in_Sage,
            [Description("Direct delivery detail")]
            sp_Direct_delivery_detail,
            [Description("Duplicated Orders")]
            sp_DuplicatedOrders,
            [Description("Duplicated Products On Orders")]
            sp_DuplicatedProductsOnOrders,
            [Description("Display Orders")]
            sp_Display_Orders,
            [Description("Display Orders 2")]
            sp_Display_Orders2,
            [Description("Forest Customer List")]
            sp_Forest_Customer_List,
            [Description("Forest Orders For Bulk")]
            sp_Forest_Orders_For_Bulk,
            [Description("Forest Split Back Orders")]
            sp_Forest_Split_Back_Orders, 
            [Description("Forward Order Exception")]
            sp_Forward_Order_Exception_Report,
            [Description("Gavins Reconcilliation")]
            sp_Gavins_Reconcilliation,
            [Description("Get Orders Processed To Reconcile")]
            sp_GetOrdersProcessedToReconcile,
            [Description("Get Tufnells Letters To Remove")]
            sp_GetTufnellsLettersToRemove,
            [Description("GroupReports - Supplier Payments By Supplier Type A")]
            sp_GroupReports_SupplierPaymentsBySupplierType_A,
            [Description("GroupReports - Supplier Payments By Supplier Type B")]
            sp_GroupReports_SupplierPaymentsBySupplierType_B,
            [Description("HD Order Value")]
            sp_HD_order_value,
            [Description("HD Order Value DDH")]
            sp_HD_order_value_DDH,
            [Description("HD SAM")]
            sp_HD_SAM,
            [Description("Wyevale status 5 held orders")]
            sp_Wyevale_status_5_held_orders,
            [Description("Home Delivery incoming orders for a week")]
            sp_Home_Delivery_incoming_orders_for_a_week,
            [Description("Home Delivery OOS 2")]
            sp_Home_Delivery_OOS_report,
            [Description("Home Delivery OOS")]
            sp_Home_Delivery_OOS,
            [Description("Home Delivery Total KPI")]
            sp_Home_Delivery_Total_KPI,
            [Description("Homebase orders over 28 days")]
            sp_Homebase_orders_over_28_days,
            [Description("Class by Week")]
            sp_JG_report_by_class_by_week,
            [Description("LH New Product")]
            sp_LH_New_product_report,
            [Description("Log Cabin Orders")]
            sp_Log_Cabin_Orders,
            [Description("Back orders for selected day")]
            sp_Back_orders_for_selected_day,
            [Description("List Symphony Products")]
            sp_ListSymphonyProducts,
            [Description("Lost Orders")]
            sp_lost_orders,
            [Description("Lost Orders for MCP")]
            sp_lost_orders_for_mcp,
            [Description("Magma Errors")]
            sp_MagmaErrors,
            [Description("MCP Forest")]
            sp_MCP_Forest,
            [Description("Faxes")]
            sp_MCP_Forest_Faxes,
            [Description("MCP Multiples")]
            sp_MCP_Multiples,
            [Description("New Accounts Set Up In Last 12 Months")]
            sp_NewAccountsSetUpInLast12Months,
            [Description("New Accounts Set Up Between Dates")]
            sp_NewAccountsSetUpBetweenDates,
            [Description("Home Delivery Summary")]
            sp_New_Home_Delivery_Summary_OOS,
            [Description("Linked Orders")]
            sp_New_Linked_Orders,
            [Description("Store HD out of stock")]
            sp_New_Store_HD_out_of_stock,
            [Description("Number Of Forest Orders Placed")]
            sp_Number_Of_Forest_Orders_Placed,
            [Description("Orders")]
            sp_order_report,
            [Description("Orders By User By Date")]
            sp_OrdersByUserByDate,
            [Description("Payment Category")]
            sp_Payment_Category,
            [Description("On Hold Accounts")]
            sp_Get_OnHoldAccounts,
            [Description("Order Value")]
            sp_Order_Value,
            [Description("Outstanding on Ledger")]
            sp_Outstanding_on_ledger,
            [Description("Outstanding on Ledger 2")]
            sp_Outstanding_on_ledger2,
            [Description("Peter's Panel Report")]
            sp_Panel_Report_Peter,
            [Description("Alistair's Information ss")]
            sp_Alistairs_Information_ss,
            [Description("Incoming HD Orders")]
            sp_Incoming_HD_Orders,
            [Description("Parcelnet Charges for Grattans")]
            sp_Parcelnet_Charges_GRATTANS,
            [Description("Parcelnet Charges for J.D.Williams")]
            sp_Parcelnet_Charges_JD_Williams,
            [Description("Price lists versus standard selling price")]
            sp_Price_lists_versus_standard_selling_price,
            [Description("Product Bible")]
            sp_ProductBibleByProductGroup,
            [Description("Product Images")]
            sp_Product_Images,
            [Description("Products and side line bins")]
            sp_products_and_side_line_bins,
            [Description("Reconcilliation")]
            sp_reconcilliation,
            [Description("Remittance Checker")]
            sp_RemittanceCheckerRUN,
            [Description("Require Call from Installation Team CRM")]
            sp_Require_Call_from_Installation_Team,
            [Description("Sales KPIS")]
            sp_sales_KPIS_2,
            [Description("Sales Invoicing")]
            sp_Sales_Invoicing,
            [Description("FOC Report")]
            sp_SAMS_NEW_FOC_REPORT,
            [Description("Shedstore FOC Orders")]
            sp_Shedstore_FOC_Orders,
            [Description("Shedstore Invoice New")]
            sp_Shedstore_Invoice_New,
            [Description("Show Customer Order Info For Claims")]
            sp_Fetch_CustomerOrderNumber2_ForCustomer,
            [Description("Spare part at given status")]
            sp_Spare_part_at_given_status,
            [Description("Spare part back order")]
            sp_Spare_part_back_order,
            [Description("Spare part back order new")]
            sp_Spare_part_back_order_new,
            [Description("Spare part from order number")]
            sp_Spare_part_from_order_number,
            [Description("Spare part orders held")]
            sp_Spare_part_orders_held,
            [Description("Spare part overview")]
            sp_Spare_part_overview,
            [Description("Spare parts")]
            sp_spare_parts,
            [Description("Split Bundle Line Check")]
            sp_Split_Bundle_Line_Check,
            [Description("Stock Enquiry")]
            sp_Stock_Enquiry,
            [Description("Stockist Search")]
            sp_Stockist_Search,
            [Description("Stock Enquiry By Product Group")]
            sp_Stock_Enquiry_By_Product_Group,
            [Description("Stock Report Showing DELGRP and DELNTSeq")]
            sp_Stock_Report_Showing_DELGRP_and_DELNTSeq,
            [Description("Symphony Orders Between Dates")]
            sp_SymphonyOrdersBetweenDates,
            [Description("Tillington Back Order Report")]
            sp_Tillington_Back_Order_Report,
            [Description("TP Quantity Check")]
            sp_TP_Quantity_Check,
            [Description("TP Warrington Back Orders")]
            sp_TP_Warrington_Back_Orders,
            [Description("TP Warrington Orders")]
            sp_TP_Warrington_Orders,
            [Description("Travis Perkins Back Orders")]
            sp_Travis_Perkins_Back_Orders,
            [Description("Tuffnells Unallocated Delivery Dates")]
            sp_TuffnellsUnallocatedDeliveryDates,
            [Description("Tuffnells Orders")]
            sp_Tuffnells_Orders_New,            
            [Description("Update Bundle")]
            sp_UpdateBundle,
            [Description("Vickys Query")]
            sp_Vickys_Query,
            [Description("Web KPI Orders Processed")]
            sp_Web_KPI_orders_processed,
            [Description("Web KPI Spare Parts Processed")]
            sp_Web_KPI_spare_parts_processed,
            [Description("WH02 Stock Movement Report")]
            sp_wh02_Stock_Movement_Report,
            [Description("WH21 Decking Components")]
            sp_wh21_decking_components,
            [Description("WH21 Decking Fixings")]
            sp_wh21_decking_fixings,
            [Description("WH21 Lap Panels")]
            sp_wh21_lap_panels,
            [Description("WH21 Split Back Order")]
            sp_wh21_split_back_order,
            [Description("WH21 Summary")]
            sp_wh21_summary,
            [Description("WH21 Summer Houses")]
            sp_wh21_summer_houses,
            [Description("WH21 Total Back Order")]
            sp_wh21_total_back_order,
            [Description("WH21 Total Back Order Summary")]
            sp_wh21_total_back_order_summary,
            [Description("WH88 Stock With No Orders")]
            rpt_WH88_Stock_With_No_Orders,
            [Description("Wickes Ordering")]
            sp_Wickes_Ordering,
            [Description("Wickes Ordering 2")]
            sp_Wickes_Ordering1,
            [Description("Wickes Stores Not Ordering")]
            sp_Wickes_Stores_Not_Ordering,
            [Description("Worst Offending spare parts")]
            sp_Worst_Offending_spare_parts,
            [Description("Worst Offending spare parts detail")]
            sp_Worst_Offending_spare_parts_detail,
            [Description("Z044 and Z046 FOC")]
            sp_Z044_AND_Z046_FOC,
            [Description("Obsolete Stock")]
            rpt_Obsolete_Stock,
            [Description("CX Processing")]
            rpt_cx_processing,
            [Description("Stock Report MM")]
            rpt_StockReport_MM,
            [Description("Locking")]
            sp_WhosLocking           

        }

        public enum ConnectionStrings
        {
            [Description("ForestServicesConnectionString")]
            ForestServicesConnectionString,
            [Description("cs3liveConnectionString")]
            cs3liveConnectionString,
            [Description("ADConnectionString")]
            ADConnectionString,
            [Description("cs3liveApp01ConnectionString")]
            cs3liveApp01ConnectionString
            //[Description("SymphonyConnectionString")]
            //SymphonyConnectionString           
        }


        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = 
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if(!type.IsEnum) throw new InvalidOperationException();
            foreach(var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if(attribute != null)
                {
                    if(attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if(field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }
    }

    public class size
    {
        public const int numberOfExtensionAttributesUsed = 4;

    }
}