﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using ReportGenerator.Models;
using ReportGenerator.ViewModel.Report;


namespace ReportGenerator.Controllers
{
    public class ReportController : Controller
    {
        CS3 context = new CS3();


       
        public static object Definitions { get; private set; }

        // GET: Report
        public ActionResult Index()
        {
            transactionTypesViewModel viewModel = new transactionTypesViewModel();


            //operatorList.Select(p => p.OperatorType).Distinct();
            var transactionTypes = context.stkhstms.Select(tt => tt.transaction_type).Distinct().ToList();

            //viewModel.transactionType = transactionTypes.ToList();
          ViewData["tt"] = transactionTypes;


            return View(ViewData["tt"]);
        }

        public ActionResult ReportList()
        {

            List<string> reportlist = GetAllAvailableReports();

            //List<string> list = from e in XDocument.Load("..\\..\\AllAvailableReports.xml");
            //                    select e;







            ////string xml = "<Ids><id>1</id><id>2</id></Ids>";


            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(@"../App_Data/AllAvailableReports.xml");

            //XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/Product");

            //XDocument doc = XDocument.Parse(xmlDoc);

            //List<string> list = doc.Root.Elements("id")
            //             .Select(e => e.Value)
            //             .ToList();

            ViewBag.reportlist = reportlist;
            return View();
        }


        public static List<string> GetAllAvailableReports()
        {
            return GetAllAvailableReportsByGroup();
        }


        public static List<string> GetAllAvailableReportsByGroup()
        {
           var reportNamesList = new List<string>();

          //string XMLfile = System.Web.HttpContext.Current.Server.MapPath(Definitions.GetEnumDescription(Definitions.MapPaths.AllAvailableReports));


            string XMLfile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/AllAvailableReports.xml");
            DataSet ds = new DataSet();
            // Create new FileStream with which to read the schema.
            System.IO.FileStream fsReadXml = new System.IO.FileStream
                (XMLfile, System.IO.FileMode.Open);

            ds.ReadXml(fsReadXml);

            fsReadXml.Close();

            DataView dv = ds.Tables[0].DefaultView;

            DataTable dt = dv.ToTable();


            foreach (DataRow row in dt.Rows)
            {
                reportNamesList.Add(row["ID"].ToString());
            }


            return reportNamesList;
        }
    }
}