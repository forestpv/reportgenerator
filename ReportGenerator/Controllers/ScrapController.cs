﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportViewerForMvc;
using ReportGenerator.Reports;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace ReportGenerator.Controllers
{
    public class ScrapController : Controller
    {
        // GET: Scrap
        public ActionResult Index()
        {
            return View();
        }

        //ForestServices ds = new ForestServices();

        public ActionResult ScrapReport()
        {
            //ReportViewerForMvc.ReportViewerWebForm reportViewer = new ReportViewerForMvc();
            //reportViewer.ProcessingMode = ProcessingMode.Local;
            //reportViewer.SizeToReportContent = true;
            //reportViewer.Width = Unit.Percentage(900);
            //reportViewer.Height = Unit.Percentage(900);

            var connectionString = ConfigurationManager.ConnectionStrings["ForestServicesConnectionString"].ConnectionString;


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("sp_ManAcc_Scrap_Report", connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Connection.Open();
            da.SelectCommand.CommandTimeout = 0;

            da.Fill(dt);





            //SqlConnection conx = new SqlConnection(connectionString);
            //using (SqlCommand cmd = new SqlCommand("sp_ManAcc_Scrap_Report", conx))
            //{

            //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
            //    adp.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            //    //adp.Fill(ds, ds.Employee_tbt.TableName);


            //    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\Report3.rdlc";
            //    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ForestServices", ds.Tables[0]));


            //    ViewBag.ReportViewer = reportViewer;
            //}
            

           

            return View();
        }
    }
}